[![pipeline status](https://gitlab.com/stim-awesome/stim-awesome/badges/master/pipeline.svg)](https://gitlab.com/stim-awesome/stim-awesome/commits/master)
[![coverage report](https://gitlab.com/stim-awesome/stim-awesome/badges/master/coverage.svg)](https://gitlab.com/stim-awesome/stim-awesome/commits/master)

# Stim Awesome

Stim Awesome wants to be a comprehensive, accurate, and up-to-date directory of [Stimulus JS](https://stimulusjs.org) controllers.

With the initial release of Stimulus JS into the world, there is a need to aggregate reusable controllers so that the entire community can benefit from them. Until Stim Awesome, there was no way to locate these existing controllers, so all functionality outside of the [Stimulus JS Handbook](https://stimulusjs.org/handbook) was left as an exercise for the reader.

## Where does the data come from?

[The NPM Package Registry](https://npmjs.com/).

## Acknowledgements

Much inspiration for this website, both in conception and design, came from  [Vim Awesome](https://github.com/vim-awesome/vim-awesome), a resource for browsing Vim plugins.
