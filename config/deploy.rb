# frozen_string_literal: true

# Lock Capistrano so that it only works when the Gem is on one version
lock "~> 3.11.0"

set :application, "stimawesome"
set :repo_url, "git@gitlab.com:stim-awesome/stim-awesome.git"
set :deploy_to, "/home/deploy/stimawesome"
append :linked_dirs,
  "log",
  "tmp/pids",
  "tmp/cache",
  "tmp/sockets",
  "vendor/bundle",
  "public/system",
  "public/uploads",
  ".bundle"

# Manually verify the host key before first deploy.
set :ssh_options, verify_host_key: :always

# Link the master.key file for secret credentials
append :linked_files, "config/master.key"

# Ensure that we can clean up stale instance directories, which are owned by the root user.
set :passenger_restart_with_sudo, true

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5
