# frozen_string_literal: true

Rails.application.routes.draw do
  root to: "packages#index"

  resources :packages, only: %i[index show new create] do
    collection do
      resources :validations, only: %i[create], module: :packages, as: :package_validations
    end
  end
end
