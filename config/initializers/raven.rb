# frozen_string_literal: true

if Rails.env.production?
  Raven.configure do |config|
    config.dsn = "https://#{Rails.application.credentials.dig(:sentry, :dsn)}@sentry.io/1206600"
  end
end
