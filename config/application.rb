# frozen_string_literal: true

require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module StimAwesome
  class Application < Rails::Application
    config.generators.assets = false # Controllers do not generate javascript/stylesheets
    config.generators.helper = false # Controllers do not generate helpers

    config.middleware.use Rack::Attack

    config.generators do |g|
      g.test_framework :rspec, # Use RSpec by default for testing
        view_specs:       false,
        request_specs:    false,
        controller_specs: false,
        routing_specs:    false

      g.fixture_replacement :factory_bot # Use FactoryBot for test fixtures
    end

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end
