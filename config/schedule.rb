# frozen_string_literal: true

# Add RBenv to the path
set :env_path, '"$HOME/.rbenv/shims":"$HOME/.rbenv/bin"'

# Redefine runner commands to use RBenv
job_type :rake,   ' cd :path && PATH=:env_path:"$PATH" RAILS_ENV=:environment bin/rake :task --silent :output '
job_type :runner, %q( cd :path && PATH=:env_path:"$PATH" bin/rails runner -e :environment ':task' :output )
job_type :script, ' cd :path && PATH=:env_path:"$PATH" RAILS_ENV=:environment bundle exec bin/:task :output '

every 1.day, at: "3:00 am" do
  runner "Package.all.map(&:update_information)"
end

every 1.day, at: "4:00 am" do
  runner "Package.all.map(&:update_score)"
end
