# frozen_string_literal: true

source "https://rubygems.org"

ruby "2.5.5"

gem "bootsnap",             "~> 1.4", require: false # Faster boot times
gem "capistrano",           "~> 3.11"   # Deployment automation
gem "capistrano-bundler",   "~> 1.6"    # Install dependencies on deploy
gem "capistrano-passenger", "~> 0.2"    # Add passenger helpers to deployment automation
gem "capistrano-rails",     "~> 1.4"    # Add Rails helpers to deployment automation
gem "capistrano-rbenv",     "~> 2.1"    # Add rbenv helpers to deployment automation
gem "faraday",              "~> 0.15.3" # HTTP client library
gem "faraday_middleware",   "~> 0.13"   # Additional capabilities for Faraday
gem "friendly_id",          "~> 5.2.5"  # Prettier URLs
gem "jbuilder",             "~> 2.9"    # Build JSON data returns easily
gem "pagy",                 "~> 3.3"    # Pagination helper
gem "pg",                   "~> 1.1.3"  # Postgres DB Driver
gem "pg_search",            "~> 2.3.0"  # Search models through Postgres
gem "puma",                 "~> 4.0.0"  # App server
gem "rack-attack",          "~> 6.1.0"  # Throttle and block abusive requests
gem "rails",                "~> 6.0.0"  # Base gem for Rails application
gem "redcarpet",            "~> 3.4.0"  # Markdown parser
gem "rouge",                "~> 3.5"    # Code syntax highlighting
gem "sassc-rails",          "~> 2.1.2"  # SCSS stylesheets
gem "sentry-raven",         "~> 2.9"    # Exception monitoring
gem "turbolinks",           "~> 5.2.0"  # Cache webpages to behave like a SPA
gem "tzinfo-data",  "~> 1.2018.4", platforms: %i[mingw mswin x64_mingw jruby] # IANA time zones
gem "uglifier",     "~> 4.1.19" # Compress JS assets
gem "validate_url", "~> 1.0.2"  # Add a url validator to ActiveRecord
gem "webpacker",    "~> 4.0.7"  # Transpile JS
gem "whenever",     "~> 1.0.0", require: false # Cron jobs for Rails

group :development, :test do
  gem "bullet",                "~> 6.0"   # Track query issues
  gem "factory_bot_rails",     "~> 5.0"   # Create data for testing
  gem "faker",                 "~> 1.9"   # Generate fake data
  gem "pry-rails",             "~> 0.3"   # Use Pry for Rails console
  gem "rspec-rails",           "~> 3.8"   # Testing framework
  gem "rspec_junit_formatter", "~> 0.4"   # RSpec results that CircleCI can read
  gem "shoulda-matchers",      "~> 4.1"   # Enhanced matchers for ActiveRecord
end

group :test do
  gem "simplecov", "~> 0.17", require: false # Code coverage tool
  gem "sinatra",   "~> 2.0" # Basic web server
  gem "webmock",   "~> 3.6" # Utility to mock out external web responses
end

group :development do
  gem "better_errors",         "~> 2.5" # Improved error page
  gem "binding_of_caller",     "~> 0.8" # Enable advanced features of Better Errors
  gem "brakeman",              "~> 4.5", require: false # Check for security vulnerabilities
  gem "erb_lint",              "~> 0.0"  # Linters for ERB code
  gem "guard",                 "~> 2.15" # Development server event handler
  gem "guard-bundler",         "~> 2.2"  # Automatically install gems when Gemfile changes
  gem "guard-livereload",      "~> 2.5"  # Reload the browser when view files change
  gem "guard-process",         "~> 1.2"  # Run arbitrary processes from Guardfile
  gem "listen",                "~> 3.1"  # Watch for file modifications
  gem "rubocop",               "~> 0.72" # Style linter
  gem "rubocop-rspec",         "~> 1.33" # Style linter
  gem "web-console",           "~> 4.0"  # Debugging console
  gem "yard",                  "~> 0.9"  # Documentation generator
end
