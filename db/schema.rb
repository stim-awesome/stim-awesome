# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_190_707_173_532) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "authors", force: :cascade do |t|
    t.string "name", null: false
    t.string "email"
    t.index ["email"], name: "index_authors_on_email", unique: true
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index %w[slug sluggable_type scope], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index %w[slug sluggable_type], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "packages", force: :cascade do |t|
    t.string "name", null: false
    t.string "version", null: false
    t.string "short_description", null: false
    t.string "long_description", null: false
    t.bigint "author_id"
    t.string "license"
    t.string "homepage_url"
    t.string "slug"
    t.float "score"
    t.datetime "created_at", default: "2018-12-15 20:38:07", null: false
    t.datetime "updated_at", default: "2018-12-15 20:38:07", null: false
    t.string "repository_url"
    t.index ["author_id"], name: "index_packages_on_author_id"
    t.index ["name"], name: "index_packages_on_name", unique: true
    t.index ["score"], name: "index_packages_on_score"
    t.index ["slug"], name: "index_packages_on_slug", unique: true
  end

  add_foreign_key "packages", "authors"
end
