# frozen_string_literal: true

class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string     :name,              null: false
      t.string     :version,           null: false
      t.string     :short_description, null: false
      t.string     :long_description,  null: false
      t.references :author,            foreign_key: true
      t.string     :license
      t.string     :homepage_url
      t.string     :slug
    end

    add_index :packages, :name, unique: true
    add_index :packages, :slug, unique: true
  end
end
