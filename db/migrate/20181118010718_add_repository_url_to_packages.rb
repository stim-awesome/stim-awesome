# frozen_string_literal: true

class AddRepositoryUrlToPackages < ActiveRecord::Migration[5.2]
  def change
    add_column :packages, :repository_url, :string
  end
end
