# frozen_string_literal: true

class AddScoreToPackages < ActiveRecord::Migration[5.2]
  def change
    add_column :packages, :score, :float
    add_index  :packages, :score, order: { column: "DESC NULLS LAST" }
  end
end
