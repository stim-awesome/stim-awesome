# frozen_string_literal: true

class AddTimestampsToPackages < ActiveRecord::Migration[5.2]
  def change
    add_timestamps(:packages, default: Time.zone.now)
  end
end
