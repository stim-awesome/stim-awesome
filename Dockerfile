FROM ruby:2.5.5

LABEL maintainer="dolan.stephen1@gmail.com"

# Get newest versions of Yarn and Node repos to install
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

# Add Ansible repo
RUN echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" | tee /etc/apt/sources.list.d/ansible.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367

# Install the following apt based dependencies required to run application:
# - Ansible for continuous infrastructure deployment
# - Build Essential for dev packages
# - PostgreSQL for gem 'pg'
# - NodeJS for JS runtime environment
# - Yarn for package installations and webpack compilation
# - OpenSSH Client for GitLab Capistrano SSH connections
# - Google Chrome for JavaScript testing
# - ImageMagick for ActiveStorage image variants
RUN apt-get update \
  && apt-get install -y --no-install-recommends --allow-unauthenticated \
  ansible \
  build-essential \
  postgresql \
  nodejs \
  yarn \
  openssh-client \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Configure the main working directory. This is the base directory used
# in any further RUN, COPY, and ENTRYPOINT commands.
RUN mkdir -p /app
WORKDIR /app

# Install gems
COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install --jobs 20 --retry 5
