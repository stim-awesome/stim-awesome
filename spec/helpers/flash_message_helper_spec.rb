# frozen_string_literal: true

require "rails_helper"

RSpec.shared_examples "a valid flash class" do |css_class, color_class|
  subject { helper.flash_class(css_class) }

  it { is_expected.to eq class_string(color_class) }

  context "with a string parameter" do
    let(:css_class) { css_class.to_s }

    it { is_expected.to eq class_string(color_class) }
  end

  # Helper to generate the Tailwind CSS for flash classes of a given color.
  #
  # @param [String] color the color for the flash class string to use.
  #
  # @return [String] a valid string of Tailwind CSS classes for a flash message.
  def class_string(color)
    background_css = "bg-#{color}-lighter"
    border_css     = "border-#{color}"
    text_css       = "text-#{color}-dark"

    "#{background_css} #{border_css} #{text_css}"
  end
end

RSpec.describe FlashMessageHelper, type: :helper do
  describe ".flash_class" do
    context 'with a valid "notice" flash class' do
      it_behaves_like "a valid flash class", :notice, "blue"
    end

    context 'with a valid "success" flash class' do
      it_behaves_like "a valid flash class", :success, "green"
    end

    context 'with a valid "alert" flash class' do
      it_behaves_like "a valid flash class", :alert, "orange"
    end

    context 'with a valid "error" flash class' do
      it_behaves_like "a valid flash class", :error, "red"
    end

    invalid_flash_types = [:bad_class, 1234, nil]
    invalid_flash_types.each do |invalid_flash_type|
      context "with an invalid '#{invalid_flash_type}' flash class" do
        it_behaves_like "a valid flash class", invalid_flash_type, "grey"
      end
    end
  end
end
