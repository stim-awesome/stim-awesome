# frozen_string_literal: true

require "rails_helper"

RSpec.describe Package, type: :model do
  subject { build(:package) }

  it { is_expected.to be_valid }

  # NAME
  it { is_expected.to have_db_column(:name) }
  it { is_expected.to have_db_index(:name).unique }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive }

  # VERSION
  it { is_expected.to have_db_column(:version) }
  it { is_expected.to validate_presence_of(:version) }

  # AUTHOR
  it { is_expected.to have_db_column(:author_id) }
  it { is_expected.to have_db_index(:author_id) }
  it { is_expected.to belong_to(:author).optional }

  # SCORE
  it { is_expected.to have_db_column(:score) }
  it { is_expected.to have_db_index(:score) }
  it { is_expected.to validate_numericality_of(:score).allow_nil.is_greater_than_or_equal_to(0) }

  # SHORT DESCRIPTION
  it { is_expected.to have_db_column(:short_description) }
  it { is_expected.to validate_presence_of(:short_description) }

  # LONG DESCRIPTION
  it { is_expected.to have_db_column(:long_description) }
  it { is_expected.to validate_presence_of(:long_description) }

  # HOMEPAGE URL
  it { is_expected.to have_db_column(:homepage_url) }
  it { is_expected.not_to allow_value("http://localhost/some_path").for(:homepage_url) }
  it { is_expected.not_to allow_value("test").for(:homepage_url) }
  it { is_expected.to allow_value("https://homepage.com").for(:homepage_url) }
  it { is_expected.to allow_value("http://homepage.com/paths").for(:homepage_url) }

  # REPOSITORY URL
  it { is_expected.to have_db_column(:repository_url) }
  it { is_expected.not_to allow_value("http://localhost/some_path").for(:repository_url) }
  it { is_expected.not_to allow_value("test").for(:repository_url) }
  it { is_expected.to allow_value("https://homepage.com").for(:repository_url) }
  it { is_expected.to allow_value("http://homepage.com/paths").for(:repository_url) }
end
