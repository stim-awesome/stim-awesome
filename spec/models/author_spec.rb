# frozen_string_literal: true

require "rails_helper"

RSpec.describe Author, type: :model do
  subject { build(:author) }

  it { is_expected.to be_valid }

  # NAME
  it { is_expected.to have_db_column(:name) }
  it { is_expected.to validate_presence_of(:name) }

  # EMAIL
  it { is_expected.to have_db_column(:email) }
  it { is_expected.to have_db_index(:email).unique }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive.allow_nil }
end
