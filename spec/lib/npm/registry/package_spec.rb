# frozen_string_literal: true

require "rails_helper"

RSpec.describe NPM::Registry::Package, type: :library do
  subject(:package) { NPM::Registry.package_from_name("test-package") }

  describe "#refine_url" do
    # Pick a random supported host for each run
    host = described_class::VALID_REPOSITORY_HOSTS.sample

    url_refinements = {
      nil                                      => nil,

      # Exercise paths
      host                                     => nil,
      "git+#{host}"                            => nil,
      "http://#{host}"                         => nil,
      "git+http://#{host}"                     => nil,
      "https://#{host}"                        => nil,
      "git+https://#{host}"                    => nil,
      "#{host}/"                               => nil,
      "#{host}////"                            => nil,
      "#{host}/something"                      => "#{host}/something",
      "#{host}/something/else"                 => "#{host}/something/else",
      "#{host}/something/else-this-thing"      => "#{host}/something/else-this-thing",
      "#{host}/something/else-123-this-thing"  => "#{host}/something/else-123-this-thing",
      "#{host}/something/else-123-this&-thing" => "#{host}/something/else-123-this",
      "#{host}/something?parameter=value"      => "#{host}/something",

      # Exercise schemes
      "http://#{host}/something"               => "http://#{host}/something",
      "http:/#{host}/something"                => "#{host}/something",
      "http//#{host}/something"                => "#{host}/something",
      "http/#{host}/something"                 => "#{host}/something",
      "git+http://#{host}/something"           => "http://#{host}/something",
      "https://#{host}/something"              => "https://#{host}/something",
      "https//#{host}/something"               => "#{host}/something",
      "https:/#{host}/something"               => "#{host}/something",
      "https/#{host}/something"                => "#{host}/something",
      "git+https://#{host}/something"          => "https://#{host}/something",

      # Exercise domains
      "http://somevalidwebsite.com/something"  => nil,
      "google.com/something"                   => nil,
    }

    url_refinements.each do |refinement|
      input  = refinement.first
      output = refinement.last

      context "with an input of #{input}" do
        it "returns #{output || "nil"}" do
          expect(package.refine_url(input)).to eq output
        end
      end
    end
  end
end
