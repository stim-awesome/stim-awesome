# frozen_string_literal: true

FactoryBot.define do
  factory :package do
    name              { Faker::Lorem.words(3).join("-") }
    version           { "#{Faker::Number.digit}.#{Faker::Number.digit}.#{Faker::Number.digit}" }
    author
    short_description { Faker::Lorem.sentence }
    long_description  { Faker::Lorem.paragraph }
  end
end
