# frozen_string_literal: true

FactoryBot.define do
  factory :author do
    name  { Faker::Name.name }
    email { Faker::Internet.unique.email }
  end
end
