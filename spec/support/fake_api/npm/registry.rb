# frozen_string_literal: true

class FakeApi
  module NPM
    # Mock out the request destinations that are hit on the NPM Registry
    class Registry < FakeApi
      get "/*" do
        json_response 200, "npm_registry_package.json"
      end
    end
  end
end
