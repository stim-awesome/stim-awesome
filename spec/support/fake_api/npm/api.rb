# frozen_string_literal: true

class FakeApi
  module NPM
    # Mock out the request destinations that are hit on the NPM API
    class Api < FakeApi
      get "/downloads/point/:period/:package" do
        json_response 200, "npm_package_downloads.json"
      end
    end
  end
end
