# frozen_string_literal: true

require "sinatra/base"

# Base class to place helpers that API mock servers can use
class FakeApi < Sinatra::Base
  # Return a JSON response loaded from a static file
  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.open(File.dirname(__FILE__) + "/fake_api/fixtures/" + file_name, "rb").read
  end
end
