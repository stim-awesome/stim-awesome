# frozen_string_literal: true

require "rails_helper"

RSpec.describe PackageScoreUpdaterService, type: :service do
  subject(:service) { described_class.new(build(:package)) }

  describe "constants" do
    it "defines a hash of valid NPM API download periods" do
      expect(described_class::DOWNLOAD_PERIOD_WEIGHTS).not_to be_empty
    end
  end

  describe ".initialize" do
    it "returns a new instance of the service" do
      expect(service).to be_a described_class
    end
  end

  describe "#perform" do
    before do
      service.perform
    end

    it "assigns a score to the service" do
      expect(service.score).not_to eq nil
    end
  end

  describe "#weighted_score" do
    let(:period) { described_class::DOWNLOAD_PERIOD_WEIGHTS.keys.sample }

    context "with a valid period" do
      it "returns a score greater than 0" do
        expect(service.weighted_score(period: period)).to be > 0
      end
    end

    context "with an invalid period" do
      let(:period) { Faker::Lorem.word }

      it "returns a score of 0" do
        expect(service.weighted_score(period: period)).to eq 0
      end
    end

    context "when a download count cannot be retrieved from the NPM API" do
      before do
        allow(NPM::Api).to receive(:downloads).and_return nil
      end

      it "returns a score of 0" do
        expect(service.weighted_score(period: period)).to eq 0
      end
    end
  end

  describe "#downloads" do
    it "returns a number of downloads greater than zero for a valid package" do
      expect(service.downloads(period: described_class::DOWNLOAD_PERIOD_WEIGHTS.keys.sample)).to be > 0
    end

    context "when the NPM API cannot find the package and returns nil for the download count" do
      before do
        allow(NPM::Api).to receive(:downloads).and_return(nil)
      end

      it "returns 0" do
        expect(service.downloads(period: described_class::DOWNLOAD_PERIOD_WEIGHTS.keys.sample)).to eq 0
      end
    end
  end
end
