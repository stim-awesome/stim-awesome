# frozen_string_literal: true

require "simplecov"
SimpleCov.start

require "spec_helper"
ENV["RAILS_ENV"] ||= "test"
require File.expand_path("../config/environment", __dir__)
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require "rspec/rails"
# Add additional requires below this line. Rails is not loaded until this point!
require "webmock/rspec"

# Load all support files
Dir[Rails.root.join("spec/support/**/*.rb")].sort.each { |f| require f }

# Checks for pending migration and applies them before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

# Disallow external connections to APIs in tests
WebMock.disable_net_connect!(allow_localhost: true)

RSpec.configure do |config|
  # Include route helpers for system tests
  config.include Rails.application.routes.url_helpers

  # Set up static tables before the suite runs
  config.before(:suite) do
    Rails.application.load_seed
  end

  # Set up some WebMock responses using basic Sinatra servers
  #
  # @see /spec/support/fake_api/**/*
  config.before(:each) do
    stub_request(:any, /registry.npmjs.org/).to_rack(FakeApi::NPM::Registry)
    stub_request(:any, /api.npmjs.org/).to_rack(FakeApi::NPM::Api)
  end

  config.include FactoryBot::Syntax::Methods
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!
  config.use_transactional_fixtures = true
  config.fail_fast                  = true
  config.formatter                  = "progress"

  Shoulda::Matchers.configure do |shoulda_config|
    shoulda_config.integrate do |with|
      with.test_framework :rspec
      with.library :rails
    end
  end
end
