# frozen_string_literal: true

# Base class for ApplicationJob.
class ApplicationJob < ActiveJob::Base
end
