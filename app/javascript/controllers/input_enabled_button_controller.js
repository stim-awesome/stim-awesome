import { Controller } from 'stimulus';

export default class extends Controller {
  static targets = ['input', 'button'];

  // Iterate over all button targets and add the disabled attribute to
  // disable them.
  _disableButtons() {
    this.buttonTargets.forEach((button) => {
      button.setAttribute('disabled', true);
    });
  }

  // Iterate over all button targets and remove the disabled attribute to
  // enable them.
  _enableButtons() {
    this.buttonTargets.forEach((button) => {
      button.removeAttribute('disabled');
    });
  }

  // Determine whether or not a given input element has a value entered.
  //
  // @param [Element] inputElement the element to check the value of.
  //
  // @return [Boolean] true if the input element has a value, otherwise false.
  _inputHasValue(inputElement) {
    return inputElement.value.trim().length > 0;
  }

  // Iterate through all inputs to determine if values are present for each one.
  // If all inputs have a value set, enable the button targets.
  // Otherwise, disable the button targets.
  _checkInputs() {
    if (this.inputTargets.every(this._inputHasValue)) {
      this._enableButtons();
    } else {
      this._disableButtons();
    }
  }

  // Add listeners to each input target for either the specified event
  // or, by default, the input event.
  _addInputListeners() {
    this.inputTargets.forEach((target) => {
      const listenedEvent = target.dataset.inputEnabledButtonEvent || 'input';

      target.addEventListener(listenedEvent, (e) => {
        const element = e.currentTarget;

        if (element.value.length === 0) {
          this._disableButtons();
        } else {
          this._checkInputs();
        }
      });
    });
  }

  // Set up listeners and establish the starting state of the buttons by running
  // an initial check on the inputs.
  initialize() {
    this._addInputListeners();
    this._checkInputs();
  }
}
