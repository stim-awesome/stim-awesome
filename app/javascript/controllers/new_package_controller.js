import { Controller } from 'stimulus';

// Layer some functionality on top of the new package submission page:
// - Dynamically show/hide the submit button based on whether or not the package is valid.
// - Enable/Disable some set of elements when the form is submitting/validating.
// - Keep the preview package name in sync with the name of the package the user is submitting.
//
// @target [Array<Element>] disableable  any number of elements to disable on form submission/validation.
// @target [Element]        loader       the loading spinner to show when the package is being validated.
// @target [Element]        form         the form that is sending the package to be validated.
// @target [Element]        namePreview  the element that is showing the package name preview.
// @target [Element]        submitButton the button that the user can click to submit a valid package.
//
// @variable [String] hiddenClass       the class to use to show and hide elements on the page.
// @variable [String] ajaxSendEvent     the event that is fired on a form when an AJAX request is sent.
// @variable [String] ajaxSuccessEvent  the event that is fired on a form when an AJAX request is successful.
// @variable [String] ajaxCompleteEvent the event that is fired on a form when an AJAX request is done.
export default class extends Controller {
  static targets = [
    'disableable',
    'loader',
    'form',
    'namePreview',
    'preview',
    'submitButton',
  ];

  // Set up some basic listeners to handle state for the entire controller.
  _addFormListeners() {
    this.formTarget.addEventListener(this.ajaxSendEvent, () => {
      this._disableElements();
      this._hideSubmitButton();
      this._hidePreview();
      this._showLoader();
    });

    this.formTarget.addEventListener(this.ajaxSuccessEvent, () => {
      this._showSubmitButton();
    });

    this.formTarget.addEventListener(this.ajaxCompleteEvent, () => {
      this._hideLoader();
      this._showPreview();
      this._enableElements();
    });
  }

  _showSubmitButton() {
    this.submitButtonTarget.classList.remove(this.hiddenClass);
    this.submitButtonTarget.removeAttribute('disabled');
  }

  _hideSubmitButton() {
    this.submitButtonTarget.classList.add(this.hiddenClass);
    this.submitButtonTarget.setAttribute('disabled', true);
  }

  _disableElements() {
    this.disableableTargets.forEach((element) => {
      element.setAttribute('disabled', true);
    });
  }

  _enableElements() {
    this.disableableTargets.forEach((element) => {
      element.removeAttribute('disabled');
    });
  }

  _showLoader() {
    this.loaderTarget.classList.remove(this.hiddenClass);
  }

  _hideLoader() {
    this.loaderTarget.classList.add(this.hiddenClass);
  }

  _showPreview() {
    this.previewTarget.classList.remove(this.hiddenClass);
  }

  _hidePreview() {
    this.previewTarget.classList.add(this.hiddenClass);
  }

  // Keep the package name in sync with the submitted package name that the user is typing.
  //
  // @param [Event] event the event that is triggering the action.
  updatePreview(event) {
    if (event.target.value.trim() !== '') {
      this.namePreviewTarget.innerHTML = event.target.value;
    } else {
      this.namePreviewTarget.innerHTML = 'an-awesome-package';
    }
  }

  // Set up the initial variables and listeners needed for the controller to work.
  initialize() {
    this.hiddenClass = 'hidden';
    this.ajaxSendEvent = 'ajax:send';
    this.ajaxSuccessEvent = 'ajax:success';
    this.ajaxCompleteEvent = 'ajax:complete';

    if (this.hasFormTarget) {
      this._addFormListeners();
    }
  }
}
