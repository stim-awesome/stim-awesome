import { Controller } from 'stimulus';

// Allow for content that should be replaced by a loader.
//
// @target [Array<Element>] content the content sections of the page that can be replaced.
// @target [Element]        loader  the loading content to show when the content is being replaced.
//
// @variable [String] hiddenClass the class to use to show and hide elements on the page.
export default class extends Controller {
  static targets = ['content', 'loader'];

  // Hide the content target and show the loader target.
  replaceContentWithLoader() {
    this.contentTargets.forEach((target) => {
      target.classList.add(this.hiddenClass);
    });

    this.loaderTarget.classList.remove(this.hiddenClass);
  }

  // Hide the loader target and show the content target.
  replaceLoaderWithContent() {
    this.loaderTarget.classList.add(this.hiddenClass);

    this.contentTargets.forEach((target) => {
      target.classList.remove(this.hiddenClass);
    });
  }

  // Set up initial variables.
  initialize() {
    this.hiddenClass = 'hidden';
  }
}
