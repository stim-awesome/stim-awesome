import { Controller } from 'stimulus';

// This controller adds the ability to remove any arbitrary object in the DOM.
//
// @example A notification with a close button:
//   <div data-controller="removable" class="notification">
//     <p>Notification content is here</p>
//     <button data-action="click->removable#remove">X</button>
//   </div>
export default class extends Controller {
  static targets = [];

  // Remove the element that defined the controller.
  //
  // We add a timeout until Stimulus 1.1 is released, to ensure that if this function is called
  // first of many actions, the other actions on the same element will have a chance to execute.
  remove() {
    setTimeout(() => {
      this.element.parentNode.removeChild(this.element);
    }, 1);
  }
}
