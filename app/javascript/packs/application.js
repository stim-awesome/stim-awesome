import { Application } from 'stimulus';
import { definitionsFromContext } from 'stimulus/webpack-helpers';
import { Placeholdable } from 'stimulus-placeholdable';
import feather from 'feather-icons';

import '../src/application.scss';

// Load all StimulusJS controllers in app/javascript/controllers
const application = Application.start();
const context = require.context('controllers', true, /.js$/);
application.load(definitionsFromContext(context));

// Load all StimulusJS controllers from packages
application.register('placeholdable', Placeholdable);

// Every time Turbolinks loads a page, show all Feather icons.
document.addEventListener('turbolinks:load', () => {
  feather.replace();
});

document.addEventListener('ajax:complete', () => {
  feather.replace();
});
