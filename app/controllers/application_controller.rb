# frozen_string_literal: true

# Base controller for the application.
class ApplicationController < ActionController::Base
  include Pagy::Backend
end
