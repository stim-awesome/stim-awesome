# frozen_string_literal: true

# Interface to the Package model.
class PackagesController < ApplicationController
  # Display a set of packages.
  def index
    @packages = Package.includes(:author)
    @packages = @packages.search(@search_text) if (@search_text = params[:q]).present?
    @packages = @packages.order(score: :desc)
    @pagy, @packages = pagy(@packages)
  end

  # Display a single package.
  def show
    @package = Package.friendly.find(params[:id])
  end

  # Initialize a new package to be created.
  def new
    @package = Package.new
  end

  # Attempt to create a new package.
  def create
    return if (validator = PackageValidatorService.validate(permitted_attributes[:name])).invalid?

    @package = Package.from_npm_registry(validator.npm_package)

    if @package.save
      flash[:success] = t("flash.package.submitted")

      redirect_to @package
    else
      render :new
    end
  end

  private

  # Allow only the name to be submitted with the package. We don't necessarily want to `params.require`,
  # because if the user somehow tries to validate an empty form we want to provide a helpful error message.
  def permitted_attributes
    params.fetch(:package, {}).permit(:name)
  end
end
