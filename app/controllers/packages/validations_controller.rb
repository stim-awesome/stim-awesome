# frozen_string_literal: true

module Packages
  # Run validations against packages to make sure they're valid for StimAwesome
  class ValidationsController < ApplicationController
    def create
      if (validator = PackageValidatorService.validate(permitted_attributes[:name])).invalid?
        @package = Package.new
        add_validator_errors_to_package(validator, @package)
        render status: :unprocessable_entity
        return
      end

      @package = Package.from_npm_registry(validator.npm_package) || Package.new
      @package.validate

      render status: :unprocessable_entity if @package.errors.any?
    end

    private

    def add_validator_errors_to_package(validator, package)
      validator.errors&.each do |message|
        package.errors[:base] << message
      end
    end

    # Allow only the name to be submitted with the package. We don't necessarily want to `params.require`,
    # because if the user somehow tries to validate an empty form we want to provide a helpful error message.
    def permitted_attributes
      params.fetch(:package, {}).permit(:name)
    end
  end
end
