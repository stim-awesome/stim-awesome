# frozen_string_literal: true

# Determine whether a package should be allowed for submission.
class PackageValidatorService
  attr_reader :package_name, :errors, :npm_package
  # Which errors can be returned with the validator.
  ERRORS = {
    package_name:           "Package name must be provided",
    already_submitted:      "Package has already been submitted",
    not_found_on_registry:  "Unable to locate NPM package by that name on the NPM registry",
    no_stimulus_dependency: "No StimulusJS dependency found in package",
  }.freeze

  def initialize(package_name:)
    @package_name = package_name
    @errors       = []
  end

  # Kick off validations for a specific package name
  def self.validate(package_name)
    validator = new(package_name: package_name)
    validator.validate

    validator
  end

  # Run all validations on a given package name.
  def validate
    @errors.push(ERRORS[:package_name])      if package_name_missing?
    @errors.push(ERRORS[:already_submitted]) if package_already_submitted?

    validate_npm_package if @errors.none?
  end

  # Run validations on a specific NPM Registry package.
  def validate_npm_package
    @npm_package = NPM::Registry.package_from_name(@package_name)

    @errors.push(ERRORS[:not_found_on_registry])  if npm_package_missing?
    @errors.push(ERRORS[:no_stimulus_dependency]) if npm_package_missing_stimulus_dependency?
  end

  def valid?
    @errors.empty?
  end

  def invalid?
    !valid?
  end

  private

  def npm_package_missing?
    @npm_package.nil?
  end

  def npm_package_missing_stimulus_dependency?
    @npm_package.present? && !@npm_package.dependent_on_stimulus?
  end

  def package_already_submitted?
    Package.with_name(@package_name).any?
  end

  def package_name_missing?
    string_nil_or_blank?(@package_name)
  end

  def string_nil_or_blank?(string)
    string.nil? || string.strip.blank?
  end
end
