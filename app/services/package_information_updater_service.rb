# frozen_string_literal: true

# This service is responsible for updating the basic information of a given
# Package instance from the NPM Registry.
class PackageInformationUpdaterService
  def initialize(package)
    @package = package
  end

  # Fetch the package from the NPM Registry by name, match it with the existing Package,
  # and update any necessary information.
  def perform
    return unless (@registry_package = NPM::Registry.package_from_name(@package.name))

    package = Package.from_npm_registry(@registry_package)

    # If, after parsing, we found the same package, make the update.
    package.save if package == @package
  end
end
