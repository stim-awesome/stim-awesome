# frozen_string_literal: true

# This service is responsible for updating the score of a given Package instance.
#
# @attr_reader [Number] score the score that the package received.
class PackageScoreUpdaterService
  attr_reader :score

  # Define how different time period download counts are weighted.
  DOWNLOAD_PERIOD_WEIGHTS = {
    "last-month" => 0.60,
    "last-week"  => 0.30,
    "last-day"   => 0.10,
  }.freeze

  def initialize(package)
    @package = package
  end

  # Iterate over supported download periods, and get a total score for the package.
  def perform
    # Get the sum of the weighted scores for each supported download period.
    @score = DOWNLOAD_PERIOD_WEIGHTS.keys.map { |period| weighted_score(period: period) }.sum

    @package.update!(score: score)
  end

  # Determine the weighted score for a package in a given period.
  #
  # @param [String] period the period to get the weighted score for.
  #
  # @return [Number] a weighted score.
  def weighted_score(period:)
    downloads(period: period) * DOWNLOAD_PERIOD_WEIGHTS.fetch(period, 0)
  end

  # Determine the downloads for a package in a given period.
  #
  # @param [String] period the period to get the downloads for from the NPM API.
  #
  # @return [Integer] the number of downloads for the package in the given period.
  def downloads(period:)
    NPM::Api.downloads(period: period, package: @package.name) || 0
  end
end
