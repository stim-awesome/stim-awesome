# frozen_string_literal: true

# Base Application Cable object.
module ApplicationCable
  # Base class for all ActionCable channels.
  class Channel < ActionCable::Channel::Base
  end
end
