# frozen_string_literal: true

# Base module for ActionCable Connections.
module ApplicationCable
  # Base class for all ActionCable Connections.
  class Connection < ActionCable::Connection::Base
  end
end
