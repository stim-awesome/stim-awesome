# frozen_string_literal: true

# Various helpers for application markdown parsing and rendering.
module MarkdownHelper
  require "redcarpet"
  require "rouge"
  require "rouge/plugins/redcarpet"

  # Child class to add some Rogue syntax highlighting to Redcarpet
  class HTML < Redcarpet::Render::HTML
    include Rouge::Plugins::Redcarpet
  end

  # A helper to parse markdown-formatted text into HTML.
  def markdown(text)
    options = {
      filter_html:     true,
      hard_wrap:       true,
      link_attributes: { rel: "nofollow", target: "_blank" },
    }

    extensions = {
      autolink:           true,
      fenced_code_blocks: true,
      lax_spacing:        true,
      no_intra_emphasis:  true,
      strikethrough:      true,
      superscript:        true,
    }

    renderer = HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)
    markdown.render(text)
  end
end
