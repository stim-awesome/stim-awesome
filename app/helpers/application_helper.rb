# frozen_string_literal: true

# Base class for all application helpers
module ApplicationHelper
  include Pagy::Frontend
end
