# frozen_string_literal: true

# Helpers to ensure that flash messages are properly displayed.
module FlashMessageHelper
  # Turn a flash message type into a set of Tailwind classes.
  #
  # @param [Symbol] flash_type the type of flash message
  #
  # @example
  #   #=> flash_type = :notice
  #   #=> flash_class(flash_type)
  #   #=> "bg-blue-lighter border-blue text-blue-dark"
  #
  # @return [String] the color-specific classes to use for the flash alert
  def flash_class(flash_type)
    background_modifier = "-lighter"
    text_modifier       = "-dark"

    case flash_type.to_s
      when "notice"  then "bg-blue#{background_modifier} border-blue text-blue#{text_modifier}"
      when "success" then "bg-green#{background_modifier} border-green text-green#{text_modifier}"
      when "alert"   then "bg-orange#{background_modifier} border-orange text-orange#{text_modifier}"
      when "error"   then "bg-red#{background_modifier} border-red text-red#{text_modifier}"
      else "bg-grey#{background_modifier} border-grey text-grey#{text_modifier}"
    end
  end
end
