# frozen_string_literal: true

# Base class for all mailers that the application uses
class ApplicationMailer < ActionMailer::Base
  default from: "from@example.com"
  layout "mailer"
end
