# frozen_string_literal: true

# An Author is the generous creator of an open-source Package.
class Author < ApplicationRecord
  validates :name,
    presence: true

  validates :email,
    uniqueness: {
      allow_nil:      true,
      case_sensitive: false,
    }

  # Find an existing Author or create a new instance based on the content of a response
  # from the NPM Registry.
  #
  # @param [NPM::Registry::Package] registry_package the package returned from the NPM repository
  #
  # @return [Author] a new or existing Stim Awesome Package Author
  def self.from_npm_registry(registry_package)
    return unless registry_package

    author_name     = registry_package.author.dig("name")
    author_email    = registry_package.author.dig("email")

    # Search by email first, since that is unique (but optional)
    existing_author = Author.find_by(email: author_email) if author_email.present?
    return existing_author if existing_author.present?

    # If no one was found by email, search for authors with the same name and no email
    # If we find one, update the email. If not, initialize a brand new Author.
    Author.where(name: author_name, email: nil).first_or_initialize.tap do |author|
      author.email = author_email
    end
  end
end
