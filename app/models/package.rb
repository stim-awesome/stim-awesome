# frozen_string_literal: true

# A Package is an awesome, shareable controller that someone in this fantastic community has created.
#
# The goal for now is that all packages will be hosted on NPM, and we will be able to
# scrape everything that we need from that source for other metadata.
#
# Keep in mind that this is, at its worst, a stale mirror of the NPM registry.
# Many attributes are stored locally to reduce the number of external dependencies on each page load.
class Package < ApplicationRecord
  include PgSearch
  extend FriendlyId
  friendly_id :name

  after_create :update_score

  pg_search_scope :search,
    against: {
      name:              "A",
      short_description: "B",
    },
    using:   {
      tsearch: {
        prefix:     true,
        dictionary: :english,
      },
    }

  scope :with_name, ->(name) { where("name ILIKE ?", name) }

  belongs_to :author,
    autosave: true,
    optional: true

  validates :name,
    presence:   true,
    uniqueness: { case_sensitive: false }

  validates :version,
    presence: true

  validates :short_description,
    presence: true

  validates :long_description,
    presence: true

  validates :score,
    numericality: {
      allow_nil:                true,
      greater_than_or_equal_to: 0,
    }

  validates :homepage_url,
    url: {
      allow_nil: true,
      no_local:  true,
      message:   I18n.t("activerecord.errors.models.package.attributes.homepage_url.url"),
    }

  validates :repository_url,
    url: {
      allow_nil: true,
      no_local:  true,
      message:   I18n.t("activerecord.errors.models.package.attributes.repository_url.url"),
    }

  # Get the full NPM URL for the package.
  #
  # @return [String] the URL of the package on npmjs.com
  def registry_url
    return nil if name.nil?

    "https://npmjs.com/package/#{name}"
  end

  # Find an existing Package or create a new instance based on the content of a response
  # from the NPM Registry.
  #
  # @param [NPM::Registry::Package] registry_package the package returned from the NPM repository
  #
  # @return [Package] a new or existing Stim Awesome Package
  def self.from_npm_registry(registry_package)
    return unless registry_package

    Package.where(name: registry_package.name).first_or_initialize.tap do |package|
      package.name              = registry_package.name
      package.version           = registry_package.latest_version
      package.author            = Author.from_npm_registry(registry_package)
      package.license           = registry_package.license
      package.short_description = registry_package.description
      package.long_description  = registry_package.readme
      package.homepage_url      = registry_package.homepage
      package.repository_url    = registry_package.repository_url
    end
  end

  # Kick off the process to update the Package score.
  def update_score
    PackageScoreUpdaterService.new(self).perform
  end

  # Kick off the process to update the Package basic information.
  def update_information
    PackageInformationUpdaterService.new(self).perform
  end
end
