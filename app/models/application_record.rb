# frozen_string_literal: true

# Base class for all application models
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  before_validation :empty_to_nil

  private

  # For every model, if there is a field that is empty, set it to nil.
  # This avoids things like empty strings being saved into the database.
  def empty_to_nil
    # Obtain the columns from a model that are of type string
    nullable_string_attribute_columns = self
      .class
      .columns
      .select { |column| column.type == :string }
      .map(&:name)

    # Obtain the attribute name and value pairs of only the columns that are of type string
    nullable_string_attributes = attributes.slice(*nullable_string_attribute_columns)

    # Set any emtpy string values to nil instead to keep the database clean
    nullable_string_attributes.each_pair do |name, value|
      # Need to use empty? instead of blank? so that we don't change 'false' to nil in params
      send("#{name.to_sym}=", nil) if value.try(:empty?)
    end
  end
end
