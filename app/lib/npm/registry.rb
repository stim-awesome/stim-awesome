# frozen_string_literal: true

module NPM
  # The NPM Registry contains all of the NPM packages.
  class Registry
    # The URI that all requests to the NPM registry must stem from.
    BASE_API_URI = "https://registry.npmjs.org"

    # Set a base URL to the NPM Registry, where we will fetch package information.
    @connection = Faraday.new BASE_API_URI do |conn|
      conn.response :json
      conn.adapter  Faraday.default_adapter
    end

    # Retrieve the JSON representation of a package on the NPM Registry.
    #
    # @param [String] package_name the name of the package to retrieve from the NPM Registry.
    #
    # @return [NPM::Registry::Package] the package with associated data from the NPM Registry.
    def self.package_from_name(package_name)
      response = nil

      begin
        response = @connection.get("/#{package_name}")
      rescue URI::InvalidURIError
        return nil
      end

      return nil unless response.success?

      NPM::Registry::Package.new(response.body)
    end
  end
end
