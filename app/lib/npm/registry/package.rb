# frozen_string_literal: true

module NPM
  class Registry
    # An NPM::Registry::Package abstracts away some of the complexity that comes with JSON response
    # parsing.
    #
    # @attr_reader [String] name the name of the NPM Package
    # @attr_reader [String] readme the README for the NPM Package
    # @attr_reader [String] description the short description of the NPM Package
    # @attr_reader [Hash]   author a hash containing information about the author of the NPM Package
    # @attr_reader [String] license the license for the NPM Package
    # @attr_reader [String] latest_version the latest version of the NPM Package
    # @attr_reader [Hash]   dependencies a hash of all production dependencies for the NPM Package
    # @attr_reader [String] homepage the homepage for the NPM Package
    class Package
      attr_reader :json_data
      attr_reader :name
      attr_reader :readme
      attr_reader :description
      attr_reader :author
      attr_reader :license
      attr_reader :latest_version
      attr_reader :dependencies
      attr_reader :homepage
      attr_reader :repository_url

      # Which repository hosts a Package URL can specify
      VALID_REPOSITORY_HOSTS = %w[github.com bitbucket.org gitlab.com].freeze

      # Initialize a new Package directly from an NPM Registry package lookup JSON.
      #
      # @param [Hash] json_body the JSON-parsed body from an NPM Registry package lookup
      #
      # @return [NPM::Registry::Package] an instance of an NPM Package from the NPM registry
      def initialize(json_body)
        @json_data      = json_body
        @latest_version = extract_latest_version
        @repository_url = extract_repository_url
        package         = @json_data.dig("versions", @latest_version)

        # Directly map a subset of package keys to instance attributes:
        instance_attributes = %w[name description author license dependencies homepage readme]
        instance_attributes.each do |attribute|
          instance_variable_set("@#{attribute}", package&.dig(attribute))
        end

        # If the README couldn't be pulled from the specific version, try to pull from the root node.
        extract_readme if @readme.blank?
      end

      # Check the dependencies of an NPM Package to ensure it is actually a Stimulus package.
      #
      # @return [Boolean] true if the package has a StimulusJS dependency, otherwise false.
      def dependent_on_stimulus?
        @dependencies&.key?("stimulus") || false
      end

      # Pull out the latest version from the package JSON.
      def extract_latest_version
        @json_data.dig("dist-tags", "latest")
      end

      # Pull out the README from the package JSON.
      def extract_readme
        @readme = @json_data.dig("readme")
      end

      # Pull out the Repository URL from the package JSON.
      def extract_repository_url
        refine_url(@json_data.dig("repository", "url"))
      end

      # Given some URL, turn it into a basic version that is visitable in a browser.
      #
      # @param [String] url the URL to refine
      #
      # @return [String, nil] either the refined URL, or nil if an invalid URL was provided.
      #
      # @example
      #   refine_url('https://github.com/stim-awesome/stim-awesome')
      #   #=> 'https://github.com/stim-awesome/stim-awesome'
      #
      #   refine_url('git+http://gitlab.com/repository')
      #   #=> 'http://gitlab.com/repository'
      #
      #   refine_url('http://gitlab.com/repository?some=parameter')
      #   #=> 'http://gitlab.com/repository'
      #
      #   refine_url('absolute_garbage')
      #   #=> nil
      def refine_url(url)
        valid_web_url_pattern = %r{
          (?<protocol>https?:\/\/)?
          (?<subdomain>\w*)
          (?<domain>#{VALID_REPOSITORY_HOSTS.join('|')})
          (?<required_path>\/[\-\w]+)
          (?<optional_paths>[\-\/\w]*)
        }x

        url&.match(valid_web_url_pattern)&.captures&.join
      end
    end
  end
end
