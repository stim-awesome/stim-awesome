# frozen_string_literal: true

# The NPM module wraps behavior for NPM packages and API endpoints.
module NPM
  # A wrapper for calls to the NPM API.
  class Api
    # The base URI that all requests to the API must stem from
    BASE_API_URI = "https://api.npmjs.org"

    # Set a base URL to the NPM API, where we will fetch download information for packages.
    @connection = Faraday.new BASE_API_URI do |conn|
      conn.response :json
      conn.adapter  Faraday.default_adapter
    end

    # Determine the downloads for a package in a given period.
    #
    # @see https://github.com/npm/registry/blob/master/docs/download-counts.md
    #
    # @param [String] period the period to fetch downloads in.
    # @param [Package] package the package to fetch downloads for.
    #
    # @return [String] the number of downloads for the package.
    def self.downloads(period:, package:)
      response = nil

      begin
        response = @connection.get("/downloads/point/#{period}/#{package}")
      rescue URI::InvalidURIError
        return
      end

      return unless response.success?

      response.body.dig("downloads")
    end
  end
end
